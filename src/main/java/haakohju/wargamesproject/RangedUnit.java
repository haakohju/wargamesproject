package haakohju.wargamesproject;

/**
 * Class RangedUnit, an extension of the superclass Unit.
 * @author Håkon Juell
 */
public class RangedUnit extends Unit{

    private int hp2 = 100;

    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    @Override
    public int getAttackBonus() {
        return 3;
    }

    /**
     * The unit gets a resistbonus of 6 for the 1st attack, decreasing to 4 for the 2nd attack, and finally decreasing to 2 for all the next attacks.
     * @return
     */
    @Override
    public int getResistBonus() {
        int hp1 = getHealth();
        int resistBonus = 2;
        if (hp1 == 100) {
            return resistBonus + 4;
        } else if (hp1 < hp2) {
            hp2 = 0;
            return resistBonus + 2;
        } else {
            return resistBonus;
        }
    }

    /**
     * Method to define the units defence bonuses depending on the terrain it will fight on.
     * So this unit gets +2 attack bonus while fighting in a forest,
     * +5 while fighting on hills, and no bonus while fighting on plains.
     * @param terrain The terrain that the unit will fight on.
     * @return Returns how much defence bonus the unit gets depending on the terrain parameter.
     */
    @Override
    public int getTerrainAttackBonus(String terrain) {
        int terrainAttackBonus = 0;
        if (terrain.equals("Forest")) {
            terrainAttackBonus = 2;
            return terrainAttackBonus;
        } else if (terrain.equals("Hills")) {
            terrainAttackBonus = 5;
            return terrainAttackBonus;
        } else if (terrain.equals("Plains")) {
            terrainAttackBonus = 0;
            return terrainAttackBonus;
        } else {
            return 0;
        }
    }

    /**
     * Method to define the units defence bonuses depending on the terrain it will fight on.
     * So this unit gets no extra defence bonus on any terrain.
     * @param terrain The terrain that the unit will fight on.
     * @return Returns how much defence bonus the unit gets depending on the terrain parameter.
     */
    @Override
    public int getTerrainDefenceBonus(String terrain) {
        return 0;
    }
}