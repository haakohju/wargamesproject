package haakohju.wargamesproject;

import com.opencsv.CSVWriter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class used for file handling.
 * @author Håkon Juell
 */
public class WriteCSV {
    /**
     * Method to write the unit list from an army to a .csv file
     * The intended use for this method specifically is to save information about army 1.
     * @param army1
     * @throws IOException
     */
    public void writeToFile1(String army1) throws IOException {
        try {
            CSVWriter writer = new CSVWriter(new FileWriter("src/main/java/File writer/Army1.csv"));
            writer.writeNext(new String[]{army1});
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to write the unit list from an army to a .csv file
     * The intended use for this method specifically is to save information about army 2.
     * @param army2
     * @throws IOException
     */
    public void writeToFile2(String army2) throws IOException {
        try {
            CSVWriter writer = new CSVWriter(new FileWriter("src/main/java/File writer/Army2.csv"));
            writer.writeNext(new String[]{army2});
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to read the information from a .csv file and return it as a string
     * @return
     */
    public String readFile1() {

        String path = "src/main/java/File writer/Army1.csv";
        String line = "";
        String list = "";

        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));

            while ((line = reader.readLine()) != null) {
                list += line.replace("\"", "").replace("[", "").replace("]", "") + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Method to read the information from a .csv file and return it as a string
     * @return
     */
    public String readFile2() {
        String path = "src/main/java/File writer/Army2.csv";
        String line = "";
        String list = "";

        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));

            while ((line = reader.readLine()) != null) {
                list += line.replace("\"", "").replace("[", "").replace("]", "") + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
