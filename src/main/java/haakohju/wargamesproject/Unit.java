package haakohju.wargamesproject;

/**
 * Superclass unit, which contains most of the basic logic that is shared between all the units.
 * @author Håkon Juell
 */
public abstract class Unit {
    private String name;
    private int health;
    private int attack;
    private int armor;

    /**
     * Constructor
     * @param name A short descriptive name of the unit.
     * @param health A value for the units health
     * @param attack A value for the units offensive power
     * @param armor A value for the units defensive power
     */
    public Unit(String name, int health, int attack, int armor) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Method for one unit to attack another.
     * @param opponent
     */
    public void attack(Unit opponent) {
        opponent.health = opponent.health - (this.attack + this.getAttackBonus()) + (opponent.armor + opponent.getResistBonus());
    }

    /**
     * Method for one unit to attack another, but with the added parameter of terrain.
     * @param opponent
     * @param terrain
     */
    public void attackWithTerrain(Unit opponent, String terrain) {
        opponent.health = opponent.health - (this.attack + this.getAttackBonus() + this.getTerrainAttackBonus(terrain)) +
                (opponent.armor + opponent.getResistBonus() + opponent.getTerrainDefenceBonus(terrain));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public abstract int getAttackBonus();

    public abstract int getResistBonus();

    public abstract int getTerrainAttackBonus(String terrain);

    public abstract int getTerrainDefenceBonus(String terrain);

    @Override
    public String toString() {
        return "\n" + name + ", " +
                "health: " + health + ", " +
                "attack: " + attack + ", " +
                "armor: " + armor;
    }
}
