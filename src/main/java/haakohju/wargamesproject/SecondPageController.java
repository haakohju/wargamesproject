package haakohju.wargamesproject;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

/**
 * Controller for the second page of the application.
 * This class does most of the user interaction, and contains most of the logic that the user interacts with.
 * @author Håkon Juell
 */
public class SecondPageController {

    private final Army army1 = new Army("Army 1");
    private final Army army2 = new Army("Army 2");
    private final Battle battle = new Battle(army1, army2);
    private final UnitFactory factory = new UnitFactory();
    private final WriteCSV write = new WriteCSV();

    @FXML private Label army1Info, army2Info, userFeedback;
    @FXML private TextField army1Ranged, army1Infantry, army1Cavalry, army1Commander;
    @FXML private TextField army2Ranged, army2Infantry, army2Cavalry, army2Commander;
    @FXML private Label battlefieldChoice, battleResult;
    private String terrain = "";

    @FXML ImageView terrainImage;
    Image forestTerrainImage = new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("Forest.png")));
    Image plainsTerrainImage = new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("Plains.png")));
    Image hillsTerrainImage = new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("Hill.png")));
    Image twoSwordImage = new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("twoswordpic.png")));

    /**
     * Method to start the battle.
     * Various exceptions are added.
     * Method also includes a soft reset on the program, so that the user will start with empty armies to create new ones.
     */
    @FXML
    private void startBattleButtonPressed() {
        if (!army1.hasUnits()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("Army 1 has no units!");
            battleResult.setText("Army 1 has no units!");
        } else if (!army2.hasUnits()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("Army 2 has no units!");
            battleResult.setText("Army 2 has no units!");
        } else if (terrain.isBlank()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("You must choose the\nterrain for the battlefield!");
            battleResult.setText("You must choose the\nterrain for the battlefield!");
        } else {
            Army winner = battle.simulate(terrain);
            if (winner.equals(army1)) {
                battleResult.setText("The winner is Army 1!\nWith " + army1.getUnitCount() + " remaining units.");
            } else if (winner.equals(army2)) {
                battleResult.setText("The winner is Army 2!\nWith " + army2.getUnitCount() + " remaining units.");
            }
            userFeedback.setTextFill(Paint.valueOf("#000000"));
            userFeedback.setText("Army units have\nbeen reset.");
            terrainImage.setImage(twoSwordImage);
            battlefieldChoice.setText("Battlefield:\nNot chosen");
            terrain = "";
            army1.clearArmy();
            army2.clearArmy();
            army1Info.setText(army1.getAllUnits());
            army2Info.setText(army2.getAllUnits());
            army1Ranged.clear(); army1Infantry.clear(); army1Cavalry.clear(); army1Commander.clear();
            army2Ranged.clear(); army2Infantry.clear(); army2Cavalry.clear(); army2Commander.clear();
        }
    }

    /**
     * Method to save the unit list from army 1 to a file.
     */
    public void saveArmy1ToFile() {
        try {
            write.writeToFile1(army1.toString());
            userFeedback.setTextFill(Paint.valueOf("#228B22"));
            userFeedback.setText("The current instance of\nArmy 1 has been saved\nsuccessfully to your local file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to read army 1's unit list from a file.
     */
    public void readArmy1FromFile(ActionEvent event) throws IOException {
        if (army1.hasUnits() || army2.hasUnits()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Read Army 1 from file");
            alert.setHeaderText("This action will reset your armies!");
            alert.setContentText("Do you want to continue?");
            ButtonType yesButton = new ButtonType("Yes");
            ButtonType noButton = new ButtonType("No");

            alert.getButtonTypes().setAll(yesButton, noButton);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == yesButton){
                Parent tableViewParent = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("FourthPage.fxml")));
                Scene tableViewScene = new Scene(tableViewParent);
                Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                window.setScene(tableViewScene);
                window.show();
            } else if (result.get() == noButton) {
                alert.close();
            }
        } else {
            Parent tableViewParent = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("FourthPage.fxml")));
            Scene tableViewScene = new Scene(tableViewParent);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(tableViewScene);
            window.show();
        }
    }

    /**
     * Method to save the unit list from army 2 to a file.
     */
    public void saveArmy2ToFile() {
        try {
            write.writeToFile2(army2.toString());
            userFeedback.setTextFill(Paint.valueOf("#228B22"));
            userFeedback.setText("The current instance of\nArmy 2 has been saved\nsuccessfully to your local file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to read army 2's unit list from a file.
     */
    public void readArmy2FromFile(ActionEvent event) throws IOException {
        if (army1.hasUnits() || army2.hasUnits()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Read Army 2 from file");
            alert.setHeaderText("This action will reset your armies!");
            alert.setContentText("Do you want to continue?");
            ButtonType yesButton = new ButtonType("Yes");
            ButtonType noButton = new ButtonType("No");

            alert.getButtonTypes().setAll(yesButton, noButton);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == yesButton){
                Parent tableViewParent = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("FifthPage.fxml")));
                Scene tableViewScene = new Scene(tableViewParent);
                Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                window.setScene(tableViewScene);
                window.show();
            } else if (result.get() == noButton) {
                alert.close();
            }
        } else {
            Parent tableViewParent = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("FifthPage.fxml")));
            Scene tableViewScene = new Scene(tableViewParent);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(tableViewScene);
            window.show();
        }
    }

    /**
     * Method to change the terrain for the battlefield.
     */
    public void forestButtonPressed() {
        terrainImage.setImage(forestTerrainImage);
        userFeedback.setTextFill(Paint.valueOf("#228B22"));
        battlefieldChoice.setText("Battlefield:\nForest");
        terrain = "Forest";
        userFeedback.setText("Battlefield set to Forest.");
        battleResult.setText("Army 1 vs Army 2");
    }

    /**
     * Method to change the terrain for the battlefield.
     */
    public void hillsButtonPressed() {
        terrainImage.setImage(hillsTerrainImage);
        userFeedback.setTextFill(Paint.valueOf("#228B22"));
        battlefieldChoice.setText("Battlefield:\nHills");
        terrain = "Hills";
        userFeedback.setText("Battlefield set to hills.");
        battleResult.setText("Army 1 vs Army 2");
    }

    /**
     * Method to change the terrain for the battlefield.
     */
    public void plainsButtonPressed() {
        terrainImage.setImage(plainsTerrainImage);
        userFeedback.setTextFill(Paint.valueOf("#228B22"));
        battlefieldChoice.setText("Battlefield:\nPlains");
        terrain = "Plains";
        userFeedback.setText("Battlefield set to plains.");
        battleResult.setText("Army 1 vs Army 2");
    }

    /**
     * Method to add x amount of units to the army, where x is defined by the user.
     * Various exceptions added.
     */
    public void army1RangedUnitButton() {
        if (army1Ranged.getText().isBlank()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("You have to enter how many \n units you want to add!");
        } else {
            try {
                int unitCount = Integer.parseInt(army1Ranged.getText().trim());
                army1.addAll(factory.createUnitList(unitCount, "RangedUnit", 100));
                army1Info.setText(army1.getAllUnits());
                userFeedback.setTextFill(Paint.valueOf("#228B22"));
                userFeedback.setText(unitCount + " Ranged units have been \nadded to Army 1.");
                battleResult.setText("Army 1 vs Army 2");
            } catch (NumberFormatException e) {
                userFeedback.setTextFill(Paint.valueOf("#DC143C"));
                userFeedback.setText("You must input a number!");
            }
        }
    }

    /**
     * Method to add x amount of units to the army, where x is defined by the user.
     * Various exceptions added.
     */
    public void army1InfantryUnitButton() {
        if (army1Infantry.getText().isBlank()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("You have to enter how many \n units you want to add!");
        } else {
            try {
                int unitCount = Integer.parseInt(army1Infantry.getText().trim());
                army1.addAll(factory.createUnitList(unitCount, "InfantryUnit", 100));
                army1Info.setText(army1.getAllUnits());
                userFeedback.setTextFill(Paint.valueOf("#228B22"));
                userFeedback.setText(unitCount + " Infantry units have been \nadded to Army 1.");
                battleResult.setText("Army 1 vs Army 2");
            } catch (NumberFormatException e) {
                userFeedback.setTextFill(Paint.valueOf("#DC143C"));
                userFeedback.setText("You must input a number!");
            }
        }
    }

    /**
     * Method to add x amount of units to the army, where x is defined by the user.
     * Various exceptions added.
     */
    public void army1CavalryUnitButton() {
        if (army1Cavalry.getText().isBlank()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("You have to enter how many \n units you want to add!");
        } else {
            try {
                int unitCount = Integer.parseInt(army1Cavalry.getText().trim());
                army1.addAll(factory.createUnitList(unitCount, "CavalryUnit", 100));
                army1Info.setText(army1.getAllUnits());
                userFeedback.setTextFill(Paint.valueOf("#228B22"));
                userFeedback.setText(unitCount + " Cavalry units have been \nadded to Army 1.");
                battleResult.setText("Army 1 vs Army 2");
            } catch (NumberFormatException e) {
                userFeedback.setTextFill(Paint.valueOf("#DC143C"));
                userFeedback.setText("You must input a number!");
            }
        }
    }

    /**
     * Method to add x amount of units to the army, where x is defined by the user.
     * Various exceptions added.
     */
    public void army1CommanderUnitButton() {
        if (army1Commander.getText().isBlank()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("You have to enter how many \n units you want to add!");
        } else {
            try {
                int unitCount = Integer.parseInt(army1Commander.getText().trim());
                army1.addAll(factory.createUnitList(unitCount, "CommanderUnit", 100));
                army1Info.setText(army1.getAllUnits());
                userFeedback.setTextFill(Paint.valueOf("#228B22"));
                userFeedback.setText(unitCount + " Commander units have been \nadded to Army 1.");
                battleResult.setText("Army 1 vs Army 2");
            } catch (NumberFormatException e) {
                userFeedback.setTextFill(Paint.valueOf("#DC143C"));
                userFeedback.setText("You must input a number!");
            }
        }
    }

    /**
     * Method to add x amount of units to the army, where x is defined by the user.
     * Various exceptions added.
     */
    public void army2RangedUnitButton() {
        if (army2Ranged.getText().isBlank()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("You have to enter how many \n units you want to add!");
        } else {
            try {
                int unitCount = Integer.parseInt(army2Ranged.getText().trim());
                army2.addAll(factory.createUnitList(unitCount, "RangedUnit", 100));
                army2Info.setText(army2.getAllUnits());
                userFeedback.setTextFill(Paint.valueOf("#228B22"));
                userFeedback.setText(unitCount + " Ranged units have been \nadded to Army 2.");
                battleResult.setText("Army 1 vs Army 2");
            } catch (NumberFormatException e) {
                userFeedback.setTextFill(Paint.valueOf("#DC143C"));
                userFeedback.setText("You must input a number!");
            }
        }
    }

    /**
     * Method to add x amount of units to the army, where x is defined by the user.
     * Various exceptions added.
     */
    public void army2InfantryUnitButton() {
        if (army2Infantry.getText().isBlank()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("You have to enter how many \n units you want to add!");
        } else {
            try {
                int unitCount = Integer.parseInt(army2Infantry.getText().trim());
                army2.addAll(factory.createUnitList(unitCount, "InfantryUnit", 100));
                army2Info.setText(army2.getAllUnits());
                userFeedback.setTextFill(Paint.valueOf("#228B22"));
                userFeedback.setText(unitCount + " Infantry units have been \nadded to Army 2.");
                battleResult.setText("Army 1 vs Army 2");
            } catch (NumberFormatException e) {
                userFeedback.setTextFill(Paint.valueOf("#DC143C"));
                userFeedback.setText("You must input a number!");
            }
        }
    }

    /**
     * Method to add x amount of units to the army, where x is defined by the user.
     * Various exceptions added.
     */
    public void army2CavalryUnitButton() {
        if (army2Cavalry.getText().isBlank()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("You have to enter how many \n units you want to add!");
        } else {
            try {
                int unitCount = Integer.parseInt(army2Cavalry.getText().trim());
                army2.addAll(factory.createUnitList(unitCount, "CavalryUnit", 100));
                army2Info.setText(army2.getAllUnits());
                userFeedback.setTextFill(Paint.valueOf("#228B22"));
                userFeedback.setText(unitCount + " Cavalry units have been \nadded to Army 2.");
                battleResult.setText("Army 1 vs Army 2");
            } catch (NumberFormatException e) {
                userFeedback.setTextFill(Paint.valueOf("#DC143C"));
                userFeedback.setText("You must input a number!");
            }
        }
    }

    /**
     * Method to add x amount of units to the army, where x is defined by the user.
     * Various exceptions added.
     */
    public void army2CommanderUnitButton() {
        if (army2Commander.getText().isBlank()) {
            userFeedback.setTextFill(Paint.valueOf("#DC143C"));
            userFeedback.setText("You have to enter how many \n units you want to add!");
        } else {
            try {
                int unitCount = Integer.parseInt(army2Commander.getText().trim());
                army2.addAll(factory.createUnitList(unitCount, "CommanderUnit", 100));
                army2Info.setText(army2.getAllUnits());
                userFeedback.setTextFill(Paint.valueOf("#228B22"));
                userFeedback.setText(unitCount + " Commander units have been \nadded to Army 2.");
                battleResult.setText("Army 1 vs Army 2");
            } catch (NumberFormatException e) {
                userFeedback.setTextFill(Paint.valueOf("#DC143C"));
                userFeedback.setText("You must input a number!");
            }
        }
    }

    /**
     * Method that takes the user to another scene which contains detailed information about all the different units.
     * @param event
     * @throws IOException
     */
    public void unitInfoButtonPressed(ActionEvent event) throws IOException {
        if (army1.hasUnits() || army2.hasUnits()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Show Unit Info");
            alert.setHeaderText("This action will reset your armies!");
            alert.setContentText("Do you want to continue?");
            ButtonType yesButton = new ButtonType("Yes");
            ButtonType noButton = new ButtonType("No");

            alert.getButtonTypes().setAll(yesButton, noButton);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == yesButton){
                Parent tableViewParent = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("ThirdPage.fxml")));
                Scene tableViewScene = new Scene(tableViewParent);
                Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                window.setScene(tableViewScene);
                window.show();
            } else if (result.get() == noButton) {
                alert.close();
            }
        } else {
            Parent tableViewParent = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("ThirdPage.fxml")));
            Scene tableViewScene = new Scene(tableViewParent);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(tableViewScene);
            window.show();
        }
    }
}