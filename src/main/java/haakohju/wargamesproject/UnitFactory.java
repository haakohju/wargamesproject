package haakohju.wargamesproject;

import java.util.*;

/**
 * Factory class used to create one or more units at a time.
 * @author Håkon Juell
 */
public class UnitFactory {
    /**
     * Method to create a single unit.
     * @param name
     * @param health
     * @return Returns a single unit.
     */
    public static Unit createUnit(String name, int health) {
        if (name.equalsIgnoreCase("RangedUnit")) {
            return new RangedUnit(name, health);
        } if (name.equalsIgnoreCase("InfantryUnit")) {
            return new InfantryUnit(name, health);
        } if (name.equalsIgnoreCase("CavalryUnit")) {
            return new CavalryUnit(name, health);
        } if (name.equalsIgnoreCase("CommanderUnit")) {
            return new CommanderUnit(name, health);
        } else {
            return null;
        }
    }

    /**
     * Method to create a list of units.
     * @param n
     * @param name
     * @param health
     * @return Returns the list of units.
     */
    public static ArrayList<Unit> createUnitList(int n, String name, int health) {
        if (n < 1) {
            throw new IllegalArgumentException("Amount of units can not be less than 0.");
        }

        ArrayList<Unit> unitList = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            if (name.equalsIgnoreCase("RangedUnit")) {
                unitList.add(new RangedUnit(name, health));
            } if (name.equalsIgnoreCase("InfantryUnit")) {
                unitList.add(new InfantryUnit(name, health));
            } if (name.equalsIgnoreCase("CavalryUnit")) {
                unitList.add(new CavalryUnit(name, health));
            } if (name.equalsIgnoreCase("CommanderUnit")) {
                unitList.add(new CommanderUnit(name, health));
            }
        }
        return unitList;
    }
}
