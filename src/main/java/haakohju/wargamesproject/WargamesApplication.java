package haakohju.wargamesproject;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
 * Class used to initiate the application.
 * @author Håkon Juell
 */
public class WargamesApplication extends Application {

    /**
     * Method to define the starting parameters of the application.
     * @param stage
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(WargamesApplication.class.getResource("FrontPage.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1000, 750);
        stage.setTitle("Wargames");
        Image twoSwordImage = new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("twoswordpic.png")));
        stage.getIcons().add(twoSwordImage);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Method used to initiate the application.
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }
}