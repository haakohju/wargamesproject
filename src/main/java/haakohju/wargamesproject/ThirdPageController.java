package haakohju.wargamesproject;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
 * This class is the controller for a simple info page defined in the fxml file ThirdPage.fxml, it contains some written information about the different units.
 * @author Håkon Juell
 */
public class ThirdPageController extends SecondPageController {
    /**
     * Method to return to the last scene (second page).
     * @param event
     * @throws IOException
     */
    public void unitInfoButtonPressed(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("SecondPage.fxml")));
        Scene tableViewScene = new Scene(tableViewParent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }
}
