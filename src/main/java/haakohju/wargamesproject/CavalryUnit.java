package haakohju.wargamesproject;

/**
 * Class Cavalry unit, an extension of the superclass Unit.
 * @author Håkon Juell
 */
public class CavalryUnit extends Unit{

    private int count = 0;

    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }

    /**
     * The unit gets an attack bonus of 6 for it's first attack, and an attack bonus of 2 for consecutive attacks.
     * @return
     */
    @Override
    public int getAttackBonus() {
        count++;
        if (count == 1) {
            return 6;
        } else {
            return 2;
        }

    }

    /**
     * This unit has a constant resist bonus of 1.
     * @return
     */
    @Override
    public int getResistBonus() {
        return 1;
    }

    /**
     * Method to define the units attack bonuses depending on the terrain it will fight on.
     * So this unit gets +5 attack bonus while fighting on plains.
     * @param terrain The terrain that the unit will fight on.
     * @return Returns how much attack bonus the unit gets depending on the terrain parameter.
     */
    @Override
    public int getTerrainAttackBonus(String terrain) {
        int terrainAttackBonus = 0;
        if (terrain.equals("Plains")) {
            terrainAttackBonus = 5;
            return terrainAttackBonus;
        } else {
            return 0;
        }
    }

    /**
     * Method to define the units defence bonuses depending on the terrain it will fight on.
     * So this unit gets -1 defence bonus while fighting in a forest.
     * @param terrain The terrain that the unit will fight on.
     * @return Returns how much defence bonus the unit gets depending on the terrain parameter.
     */
    @Override
    public int getTerrainDefenceBonus(String terrain) {
        int terrainDefenceBonus = 0;
        if (terrain.equals("Forest")) {
            terrainDefenceBonus = -1;
            return terrainDefenceBonus;
        } else {
            return 0;
        }
    }
}