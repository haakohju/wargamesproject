package haakohju.wargamesproject;

/**
 * class that simulates a battle between 2 armies.
 * @author Håkon Juell
 */
public class Battle {

    private Army armyOne;
    private Army armyTwo;

    /**
     * Constructor
     * @param armyOne
     * @param armyTwo
     */
    public Battle(Army armyOne, Army armyTwo) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
    }

    /**
     * Method to simulate a battle between two armies.
     * @param terrain The terrain that the battle will happen on.
     * @return Returns the army that won the battle.
     */
    public Army simulate(String terrain) {
        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            Unit army1unit = (Unit) armyOne.getRandom();
            Unit army2unit = (Unit) armyTwo.getRandom();
            army2unit.attackWithTerrain(army1unit, terrain);
            if (army1unit.getHealth() <= 0) {
                armyOne.remove(army1unit);
            }
            if (!armyOne.hasUnits()) {
                return armyTwo;
            }
            army1unit.attackWithTerrain(army2unit, terrain);
             if (army2unit.getHealth() <= 0) {
                armyTwo.remove(army2unit);
            }
             if (!armyTwo.hasUnits()) {
                 return armyOne;
             }
        }
        if (armyOne.hasUnits()) {
            return armyOne;
        } else {
            return armyTwo;
        }
    }

    @Override
    public String toString() {
        return "Battle{" +
                "armyOne=" + armyOne +
                ", armyTwo=" + armyTwo +
                '}';
    }
}
