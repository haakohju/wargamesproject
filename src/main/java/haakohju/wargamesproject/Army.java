package haakohju.wargamesproject;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class that contains a list of units that make up an army.
 * @author Håkon Juell
 */
public class Army {

    private String name;
    private ArrayList<Unit> unitList;

    /**
     * Constructor
     * @param name
     */
    public Army(String name) {
        this.name = name;
        unitList = new ArrayList<Unit>();
    }

    /**
     * Constructor
     * @param name
     * @param unitList
     */
    public Army(String name, ArrayList<Unit> unitList) {
        this.name = name;
        this.unitList = unitList;
    }

    public String getName() {
        return name;
    }

    /**
     * Method to add a single unit to the army.
     * @param unit
     */
    public void add(Unit unit) {
        unitList.add(unit);
    }

    /**
     * Method to add a list of units to the army.
     * @param input
     */
    public void addAll(ArrayList<Unit> input) {
        unitList.addAll(input);
    }

    /**
     * Method to remove a single unit from the army.
     * @param unit
     */
    public void remove(Unit unit) {
        unitList.remove(unit);
    }

    /**
     * Method to check if the army is empty.
     * @return Returns true if the army contains any units, and false if the army contains no units.
     */
    public boolean hasUnits() {
        if (unitList.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Method to get the total amount of units in the army.
     * @return
     */
    public int getUnitCount() {
        return unitList.size();
    }

    /**
     * Method to remove all units from the army.
     */
    public void clearArmy() {
        unitList.clear();
    }

    /**
     * Method to fetch the String value of a list containing all the units in the army.
     * @return
     */
    public String getAllUnits() {
        int infantryCount = 0;
        int rangedCount = 0;
        int cavalryCount = 0;
        int commanderCount = 0;

        for (Unit units : unitList) {
            if (units.getName().equals("InfantryUnit")) {
                infantryCount++;
            } else if (units.getName().equals("RangedUnit")) {
                rangedCount++;
            } else if (units.getName().equals("CavalryUnit")) {
                cavalryCount++;
            } else if (units.getName().equals("CommanderUnit")) {
                commanderCount++;
            } else {
                System.out.println("This unit is not recognized.");
            }
        }
        return "\nAll units in " + this.name + "\nRanged: " + rangedCount + "\nInfantry: " + infantryCount + "\nCavalry: " + cavalryCount + "\nCommander: " + commanderCount;
    }

    /**
     * Method to fetch a random unit from the army.
     * @return
     */
    public Object getRandom() {
        if (unitList.isEmpty()) {
            return "The unitlist is empty";
        }
        Random random = new Random();
        int randomNr = random.nextInt(unitList.size());
        Unit unit = unitList.get(randomNr);
        return unit;
    }

    /**
     * Method to get all the infantry units in the army.
     * @return
     */
    public ArrayList<Unit> getInfantryUnits() {
        ArrayList<Unit> infantryUnits;
        infantryUnits = unitList.stream().filter(units -> units.getName().equals("InfantryUnit")).collect(Collectors.toCollection(ArrayList::new));
        return infantryUnits;
    }

    /**
     * Method to get all the ranged units in the army.
     * @return
     */
    public ArrayList<Unit> getRangedUnits() {
        ArrayList<Unit> rangedUnits;
        rangedUnits = unitList.stream().filter(units -> units.getName().equals("RangedUnit")).collect(Collectors.toCollection(ArrayList::new));
        return rangedUnits;
    }

    /**
     * Method to get all the cavalry units in the army.
     * @return
     */
    public ArrayList<Unit> getCavalryUnits() {
        ArrayList<Unit> cavalryUnits;
        cavalryUnits = unitList.stream().filter(units -> units.getName().equals("CavalryUnit")).collect(Collectors.toCollection(ArrayList::new));
        return cavalryUnits;
    }

    /**
     * Method to get all the commander units in the army.
     * @return
     */
    public ArrayList<Unit> getCommanderUnits() {
        ArrayList<Unit> commanderUnits;
        commanderUnits = unitList.stream().filter(units -> units.getName().equals("CommanderUnit")).collect(Collectors.toCollection(ArrayList::new));
        return commanderUnits;
    }

    @Override
    public String toString() {
        return name + " unitList: " + unitList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(unitList, army.unitList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, unitList);
    }
}