package haakohju.wargamesproject;

/**
 * Class InfantryUnit, an extension of the superclass Unit.
 * @author Håkon Juell
 */
public class InfantryUnit extends Unit{

    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    /**
     * The unit gets a constant attack bonus of 2.
     * @return
     */
    @Override
    public int getAttackBonus() {
        return 2;
    }

    /**
     * The unit gets a constant resist bonus of 1.
     * @return
     */
    @Override
    public int getResistBonus() {
        return 1;
    }

    /**
     * Method to define the units attack bonuses depending on the terrain it will fight on.
     * So this unit gets +5 attack bonus while fighting in a forest.
     * @param terrain The terrain that the unit will fight on.
     * @return Returns how much attack bonus the unit gets depending on the terrain parameter.
     */
    @Override
    public int getTerrainAttackBonus(String terrain) {
        int terrainAttackBonus = 0;
        if (terrain.equals("Forest")) {
            terrainAttackBonus = 5;
            return terrainAttackBonus;
        } else {
            return 0;
        }
    }

    /**
     * Method to define the units defence bonuses depending on the terrain it will fight on.
     * So this unit gets +5 defence bonus while fighting in a forest.
     * @param terrain The terrain that the unit will fight on.
     * @return Returns how much defence bonus the unit gets depending on the terrain parameter.
     */
    @Override
    public int getTerrainDefenceBonus(String terrain) {
        int terrainDefenceBonus = 0;
        if (terrain.equals("Forest")) {
            terrainDefenceBonus = 5;
            return terrainDefenceBonus;
        } else {
            return 0;
        }
    }

}
