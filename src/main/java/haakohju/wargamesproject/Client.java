package haakohju.wargamesproject;

import java.io.IOException;
import java.util.*;

public class Client {

    Scanner in = new Scanner(System.in);
    Army army1 = new Army("Army 1");
    Army army2 = new Army("Army 2");
    Battle battle = new Battle(army1, army2);
    UnitFactory factory = new UnitFactory();
    WriteCSV write = new WriteCSV();

    public static void main(String[] args) {
        Client c = new Client();
        c.testData();
    }

    public void testData() {
        army1.addAll(factory.createUnitList(7,"RangedUnit", 100));
        army1.addAll(factory.createUnitList(4,"CavalryUnit", 100));

        army2.addAll(factory.createUnitList(5,"InfantryUnit", 100));
        army2.addAll(factory.createUnitList(5,"CommanderUnit", 100));

        try {
            write.writeToFile1(army1.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            write.writeToFile2(army2.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("************************");

        System.out.println(battle.simulate("Plains"));

        System.out.println("_________________________________________________________");

        System.out.println(army1.getAllUnits());
        System.out.println(army2.getAllUnits());

        System.out.println("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");


        System.out.println(write.readFile1());

    }
}