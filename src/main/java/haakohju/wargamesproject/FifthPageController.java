package haakohju.wargamesproject;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
 * Controller for the fifth page, which is just an info screen that displays the units that are read from the local file Army2.csv
 * @author Håkon Juell
 */
public class FifthPageController {

    WriteCSV reader2 = new WriteCSV();

    @FXML
    TextArea textArea;

    /**
     * Method to set the text area in this scene of the GUI to the unitlist that it reads from the file.
     * This method initializes automatically when the program switches to this scene.
     */
    public void initialize() {
        textArea.setText(reader2.readFile2());
    }

    /**
     * Method to return to the last scene.
     * @param event
     * @throws IOException
     */
    public void returnButtonPressed(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("SecondPage.fxml")));
        Scene tableViewScene = new Scene(tableViewParent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }
}
