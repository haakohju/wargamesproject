package haakohju.wargamesproject;

/**
 * Class CommanderUnit, an extension of the superclass Unit.
 * @author Håkon Juell
 */
public class CommanderUnit extends CavalryUnit{

    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public CommanderUnit(String name, int health) {
        super(name, health, 25, 10);
    }

}
