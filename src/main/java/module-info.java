module haakohju.wargamesproject {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;
    requires com.opencsv;

    opens haakohju.wargamesproject to javafx.fxml;
    exports haakohju.wargamesproject;
}