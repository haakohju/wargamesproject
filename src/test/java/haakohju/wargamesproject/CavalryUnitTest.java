package haakohju.wargamesproject;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CavalryUnitTest {

    @Test
    void getAttackBonus() {
        Unit unit1 = new RangedUnit("RangedUnit", 100);
        Unit unit2 = new CavalryUnit("CavalryUnit", 100);
        assertEquals(unit2.getAttackBonus(), 6);
        unit2.attack(unit1);
        assertEquals(unit2.getAttackBonus(), 2);
        unit2.attack(unit1);
        assertEquals(unit2.getAttackBonus(), 2);
    }

    @Test
    void getResistBonus() {
        Unit unit1 = new CavalryUnit("CavalryUnit", 100);
        assertEquals(unit1.getResistBonus(), 1);
    }

    @Test
    void getTerrainAttackBonus() {
        Unit unit1 = new CavalryUnit("CavalryUnit", 100);
        assertEquals(unit1.getTerrainAttackBonus("Forest"), 0);
        assertEquals(unit1.getTerrainAttackBonus("Hill"), 0);
        assertEquals(unit1.getTerrainAttackBonus("Plains"), 5);
    }

    @Test
    void getTerrainDefenceBonus() {
        Unit unit1 = new CavalryUnit("CavalryUnit", 100);
        assertEquals(unit1.getTerrainDefenceBonus("Forest"), -1);
        assertEquals(unit1.getTerrainDefenceBonus("Hill"), 0);
        assertEquals(unit1.getTerrainDefenceBonus("Plains"), 0);
    }
}