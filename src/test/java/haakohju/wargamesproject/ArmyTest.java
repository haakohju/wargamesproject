package haakohju.wargamesproject;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {

    private String name;
    private ArrayList<Unit> unitList;
    UnitFactory factory = new UnitFactory();

    @Test
    void getName() {
        this.name = "Test";
        assertEquals(name, "Test");
    }

    @Test
    void add() {
        unitList = new ArrayList<Unit>();
        Unit unit1 = factory.createUnit("RangedUnit", 100);
        unitList.add(unit1);
        assertEquals(unitList.size(), 1);
    }

    @Test
    void addAll() {
        unitList = new ArrayList<Unit>();
        unitList.addAll(factory.createUnitList(23,"RangedUnit", 100));
        unitList.addAll(factory.createUnitList(20,"InfantryUnit", 100));
        assertEquals(unitList.size(), 43);
    }

    @Test
    void remove() {
        unitList = new ArrayList<Unit>();
        unitList.addAll(factory.createUnitList(23,"RangedUnit", 100));
        Unit unit1 = factory.createUnit("RangedUnit", 100);
        unitList.add(unit1);
        assertEquals(unitList.size(), 24);
        unitList.remove(unit1);
        assertEquals(unitList.size(), 23);
    }

    @Test
    void hasUnits() {
        unitList = new ArrayList<Unit>();
        unitList.addAll(factory.createUnitList(23,"RangedUnit", 100));
        assertEquals(unitList.isEmpty(), false);
    }

    @Test
    void getUnitCount() {
    }

    @Test
    void clearArmy() {
        unitList = new ArrayList<Unit>();
        unitList.addAll(factory.createUnitList(23,"RangedUnit", 100));
        unitList.addAll(factory.createUnitList(20,"InfantryUnit", 100));
        assertEquals(unitList.size(), 43);
        unitList.clear();
        assertEquals(unitList.size(), 0);
    }

    @Test
    void getAllUnits() {
        unitList = new ArrayList<Unit>();
        unitList.addAll(factory.createUnitList(1,"RangedUnit", 100));
        unitList.addAll(factory.createUnitList(3,"InfantryUnit", 100));
        unitList.addAll(factory.createUnitList(7,"CavalryUnit", 100));
        unitList.addAll(factory.createUnitList(9,"CommanderUnit", 100));

        int infantryCount = 0;
        int rangedCount = 0;
        int cavalryCount = 0;
        int commanderCount = 0;

        for (Unit units : unitList) {
            if (units.getName().equals("InfantryUnit")) {
                infantryCount++;
            } else if (units.getName().equals("RangedUnit")) {
                rangedCount++;
            } else if (units.getName().equals("CavalryUnit")) {
                cavalryCount++;
            } else if (units.getName().equals("CommanderUnit")) {
                commanderCount++;
            } else {
                System.out.println("This unit is not recognized.");
            }
        }
        System.out.println("\nAll units in " + this.name + "\nRanged: " + rangedCount + "\nInfantry: " + infantryCount + "\nCavalry: " + cavalryCount + "\nCommander: " + commanderCount);
    }

    @Test
    void getRandom() {
    }

    @Test
    void getInfantryUnits() {
        unitList = new ArrayList<Unit>();
        unitList.addAll(factory.createUnitList(23,"RangedUnit", 100));
        unitList.addAll(factory.createUnitList(20,"InfantryUnit", 100));
        ArrayList<Unit> infantryUnits;
        infantryUnits = unitList.stream().filter(units -> units.getName().equals("InfantryUnit")).collect(Collectors.toCollection(ArrayList::new));
        assertEquals(infantryUnits.size(), 20);
    }

    @Test
    void getRangedUnits() {
        unitList = new ArrayList<Unit>();
        unitList.addAll(factory.createUnitList(23,"RangedUnit", 100));
        unitList.addAll(factory.createUnitList(20,"InfantryUnit", 100));
        ArrayList<Unit> rangedUnits;
        rangedUnits = unitList.stream().filter(units -> units.getName().equals("RangedUnit")).collect(Collectors.toCollection(ArrayList::new));
        assertEquals(rangedUnits.size(), 23);
    }

    @Test
    void getCavalryUnits() {
        unitList = new ArrayList<Unit>();
        unitList.addAll(factory.createUnitList(17,"CavalryUnit", 100));
        unitList.addAll(factory.createUnitList(11,"CommanderUnit", 100));
        ArrayList<Unit> cavalryUnits;
        cavalryUnits = unitList.stream().filter(units -> units.getName().equals("CavalryUnit")).collect(Collectors.toCollection(ArrayList::new));
        assertEquals(cavalryUnits.size(), 17);
    }

    @Test
    void getCommanderUnits() {
        unitList = new ArrayList<Unit>();
        unitList.addAll(factory.createUnitList(17,"CavalryUnit", 100));
        unitList.addAll(factory.createUnitList(11,"CommanderUnit", 100));
        ArrayList<Unit> commanderUnits;
        commanderUnits = unitList.stream().filter(units -> units.getName().equals("CommanderUnit")).collect(Collectors.toCollection(ArrayList::new));
        assertEquals(commanderUnits.size(), 11);
    }

    @Test
    void testToString() {
    }

    @Test
    void testEquals() {
    }

    @Test
    void testHashCode() {
    }
}