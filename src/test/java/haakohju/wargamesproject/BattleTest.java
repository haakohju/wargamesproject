package haakohju.wargamesproject;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BattleTest {

    @Test
    void simulate() {
        Army army1 = new Army("Army 1");
        Army army2 = new Army("Army 2");
        Battle battle = new Battle(army1, army2);
        UnitFactory factory = new UnitFactory();
        army1.addAll(factory.createUnitList(1,"RangedUnit", 100));
        army2.addAll(factory.createUnitList(1,"RangedUnit", 100));
        Army winner = battle.simulate("Forest");
        assertEquals(winner, army2);
        army1.addAll(factory.createUnitList(5,"InfantryUnit", 100));
        winner = battle.simulate("Plains");
        assertEquals(winner, army1);
    }

    @Test
    void testToString() {
    }
}