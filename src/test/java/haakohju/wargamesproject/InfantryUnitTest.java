package haakohju.wargamesproject;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InfantryUnitTest {

    @Test
    void getAttackBonus() {
        Unit unit1 = new InfantryUnit("InfantryUnit", 100);
        assertEquals(unit1.getAttackBonus(), 2);
    }

    @Test
    void getResistBonus() {
        Unit unit1 = new RangedUnit("RangedUnit", 100);
        Unit unit2 = new InfantryUnit("InfantryUnit", 100);
        unit1.attack(unit2);
        assertEquals(unit2.getResistBonus(), 1);
        unit1.attack(unit2);
        assertEquals(unit2.getResistBonus(), 1);
    }

    @Test
    void getTerrainAttackBonus() {
        Unit unit1 = new RangedUnit("RangedUnit", 100);
        Unit unit2 = new InfantryUnit("InfantryUnit", 100);
        unit2.attackWithTerrain(unit1, "Hill");
        assertEquals(unit2.getTerrainAttackBonus("Forest"), 5);
        assertEquals(unit2.getTerrainDefenceBonus("Forest"), 5);
        assertEquals(unit2.getTerrainAttackBonus("Plains"), 0);
        assertEquals(unit2.getTerrainAttackBonus("Hill"), 0);
        unit2.attackWithTerrain(unit1, "Forest");
        assertEquals(unit2.getResistBonus(), 1);
    }

    @Test
    void getTerrainDefenceBonus() {
        Unit unit1 = new InfantryUnit("InfantryUnit", 100);
        assertEquals(unit1.getTerrainDefenceBonus("Forest"), 5);
        assertEquals(unit1.getTerrainDefenceBonus("Hill"), 0);
        assertEquals(unit1.getTerrainDefenceBonus("Plains"), 0);
    }
}