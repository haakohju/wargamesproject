package haakohju.wargamesproject;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangedUnitTest {

    @Test
    void getAttackBonus() {
        Unit unit1 = new RangedUnit("RangedUnit", 100);
        assertEquals(unit1.getAttackBonus(), 3);
    }

    @Test
    void getResistBonus() {
        Unit unit1 = new RangedUnit("RangedUnit", 100);
        Unit unit2 = new InfantryUnit("InfantryUnit", 100);
        assertEquals(unit1.getResistBonus(), 6);
        unit2.attack(unit1);
        assertEquals(unit1.getResistBonus(), 4);
        unit2.attack(unit1);
        assertEquals(unit1.getResistBonus(), 2);
    }

    @Test
    void getTerrainAttackBonus() {
        Unit unit1 = new RangedUnit("RangedUnit", 100);
        Unit unit2 = new InfantryUnit("InfantryUnit", 100);
        assertEquals(unit1.getTerrainAttackBonus("Hills"), 5);
        unit1.attackWithTerrain(unit2, "Hills");
        assertEquals(unit1.getTerrainAttackBonus("Forest"), 2);
        assertEquals(unit1.getTerrainDefenceBonus("Forest"), 0);
        assertEquals(unit1.getTerrainAttackBonus("Plains"), 0);
        unit1.attackWithTerrain(unit2, "Forest");
        assertEquals(unit1.getResistBonus(), 6);
    }

    @Test
    void getTerrainDefenceBonus() {
        Unit unit1 = new RangedUnit("RangedUnit", 100);
        assertEquals(unit1.getTerrainDefenceBonus("Forest"), 0);
        assertEquals(unit1.getTerrainDefenceBonus("Hills"), 0);
        assertEquals(unit1.getTerrainDefenceBonus("Plains"), 0);
    }
}